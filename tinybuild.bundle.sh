#!/bin/sh

basedir=$(pwd)
tinybuilderdir="$basedir/.tinybuild"
srcdir="$tinybuilderdir/src"
pkgdir="$tinybuilderdir/pkg"
outdir="$tinybuilderdir/out"

error_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[31m error\033[0m: $1\n"
	else
		printf "[tinybuild] error: $1\n"
	fi
}

info_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[0m info: $1\n"
	else
		printf "[tinybuild] info: $1\n"
	fi
}

warn_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[33m warn\033[0m: $1\n"
	else
		printf "[tinybuild] warn: $1\n"
	fi
}

specpath="./Tinybuildfile"
if [ -n "$1" ]; then
	specpath="$1"
fi
. "$specpath"
info_log 'loaded build spec'

info_log 'create builder dirs'
mkdir -p "$tinybuilderdir"
mkdir -p "$srcdir"
mkdir -p "$pkgdir"
mkdir -p "$outdir"

info_log 'begin builder'

if [ -z "$pkgname" ]; then
	error_log 'package name not specifed'
	exit 1
fi

check_verrevarch() {
	if [ -z "$pkgver" ]; then
		error_log 'package version not specifed'
		exit 1
	fi

	if [ -z "$pkgrev" ]; then
		warn_log 'package version rev not specified'
	fi

	if [ -z "$pkgarch" ]; then
		error_log 'package architecture not specified'
		exit 1
	fi
}

case "$prep" in
	ubuntu)
		info_log 'running prep for ubuntu'
if [ -z "$prep_deps" ]; then
	info_log 'no deps specified'
else
	DEBIAN_FRONTEND=noninteractive apt-get update -q 1>/dev/null || exit 1
	DEBIAN_FRONTEND=noninteractive apt-get -yq install $prep_deps 1>/dev/null || exit 1
fi
		;;
	fedora)
		info_log 'running prep for fedora'
if [ -z "$prep_deps" ]; then
	info_log 'no deps specified'
else
	dnf install -yq $prep_deps 1>/dev/null || exit 1
fi
		;;
	custom)
		info_log 'running custom prep'
if [ -z "$prep_script" ]; then
	error_log 'custom prep script not specified'
	exit 1
fi

. "$basedir/$prep_script"
cd "$basedir"
		;;
	*)
		error_log "unknown system for preping specified ($prep)"
		exit 1
		;;
esac

case "$source" in
	local)
		info_log 'fetching source from local'
if [ -z "$source_path" ]; then
	warn_log 'no path specified, using current path'
	source_path=.
fi
info_log "copying sources from $source_path to src"
cp -r "$source_path"/* "$srcdir"
		;;
	git)
		info_log 'fetching source from git'
if [ -z "$source_repo" ]; then
	error_log 'source repo not specified'
	exit 1
fi
info_log "cloning $source_repo into src"
git clone --recurse-submodules "$source_repo" "$srcdir" 1>/dev/null || exit 1
		;;
	tgz)
		info_log 'fetching source tgz'
if [ -z "$source_tgz" ]; then
	error_log 'tgz url not specifed'
	exit 1
fi
info_log "downloading $source_tgz"
curl -Ls -o "src.tgz" "$source_tgz" 1>/dev/null || exit 1
tar -xzf "src.tgz" -C "$srcdir" 1>/dev/null || exit 1
rm src.tgz
		;;
	file)
		info_log 'fetching source file'
if [ -z "$source_file" ]; then
	error_log 'file url not specified'
fi
cd "$srcdir"
info_log "downloading source to $source_file"
curl -LOs "$source_file" 1>/dev/null || exit 1
cd "$basedir"
		;;
	*)
		error_log "unknown source fetch method specified ($source)"
		exit 1
		;;
esac

case "$build" in
	make)
		info_log 'running build with make'
cd "$srcdir"
make $build_makeparams 1>/dev/null || exit 1
cd "$basedir"
		;;
	cargo)
		info_log 'running build with cargo'
cd "$srcdir"
cargo build --release 1>/dev/null || exit 1
cd "$basedir"
		;;
	electronbuilder)
		info_log 'running build with electron builder'
cd "$srcdir"
npm ci 1>/dev/null || exit 1
npm build 1>/dev/null || exit 1
./node_modules/.bin/electron-builder $build_ebparams 1>/dev/null || exit 1
cd "$basedir"
		;;
	custom)
		info_log 'running custom build'
build
cd "$basedir"
		;;
	skip)
		info_log 'skipping build stage'
		;;
	*)
		error_log "unknown build method specified ($build)"
		exit 1
		;;
esac

case "$package" in
	tgz)
		info_log 'packaging tgz'
check_verrevarch

prep
cd "$basedir"

mkdir -p "$outdir/tgzs"
cd "$pkgdir"
tar -czf "$outdir/tgzs/$pkgname-$pkgver-${pkgrev:-1}_$pkgarch.tgz" "." 1>/dev/null
cd "$basedir"
		;;
	rpm)
		info_log 'packaging rpm'
if [ -z "$package_rpmpath" ]; then
	error_log 'path for rpm files not specifed'
	exit 1
fi

mkdir -p "$HOME/rpmbuild/SOURCES"
mkdir -p "$HOME/rpmbuild/SPECS"

mv "$srcdir/$package_rpmpath/$pkgname.spec" "$HOME/rpmbuild/SPECS"
mv "$srcdir/$package_rpmpath"/* "$HOME/rpmbuild/SOURCES"

spectool -gR "$HOME/rpmbuild/SPECS/$pkgname.spec" 1>/dev/null || exit 1
dnf builddep -yq "$HOME/rpmbuild/SPECS/$pkgname.spec" 1>/dev/null || exit 1
rpmbuild -bb "$HOME/rpmbuild/SPECS/$pkgname.spec" 1>/dev/null || exit 1

mkdir -p "$outdir/rpms"
for a in x86_64 noarch; do
	[ -d "$HOME/rpmbuild/RPMS/$a" ] && mv "$HOME/rpmbuild/RPMS/$a/"* "$outdir/rpms"
done
		;;
	deb)
		info_log 'packaging deb'
check_verrevarch

prep
cd "$basedir"

mkdir -p "$outdir/debs"
dpkg -b "$pkgdir" "$outdir/debs/$pkgname-$pkgver-${pkgrev:-1}_$pkgarch.deb" 1>/dev/null || exit 1
		;;
	skip)
		info_log 'skipping package stage'
		;;
	*)
		error_log "unknown packaging method specified ($package)"
		exit 1
		;;
esac

info_log 'returning to base dir'
cd "$basedir"

info_log 'end builder'

if [ -z "$package_rpmpath" ]; then
	error_log 'path for rpm files not specifed'
	exit 1
fi

mkdir -p "$HOME/rpmbuild/SOURCES"
mkdir -p "$HOME/rpmbuild/SPECS"

mv "$srcdir/$package_rpmpath/$pkgname.spec" "$HOME/rpmbuild/SPECS"
mv "$srcdir/$package_rpmpath"/* "$HOME/rpmbuild/SOURCES"

spectool -gR "$HOME/rpmbuild/SPECS/$pkgname.spec" 1>/dev/null || exit 1
dnf builddep -yq "$HOME/rpmbuild/SPECS/$pkgname.spec" 1>/dev/null || exit 1
rpmbuild -bb "$HOME/rpmbuild/SPECS/$pkgname.spec" 1>/dev/null || exit 1

mkdir -p "$outdir/rpms"
for a in x86_64 noarch; do
	[ -d "$HOME/rpmbuild/RPMS/$a" ] && mv "$HOME/rpmbuild/RPMS/$a/"* "$outdir/rpms"
done

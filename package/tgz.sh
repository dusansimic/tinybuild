check_verrevarch

prep
cd "$basedir"

mkdir -p "$outdir/tgzs"
cd "$pkgdir"
tar -czf "$outdir/tgzs/$pkgname-$pkgver-${pkgrev:-1}_$pkgarch.tgz" "." 1>/dev/null
cd "$basedir"

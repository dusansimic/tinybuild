check_verrevarch

prep
cd "$basedir"

mkdir -p "$outdir/debs"
dpkg -b "$pkgdir" "$outdir/debs/$pkgname-$pkgver-${pkgrev:-1}_$pkgarch.deb" 1>/dev/null || exit 1

if [ -z "$prep_deps" ]; then
	info_log 'no deps specified'
else
	DEBIAN_FRONTEND=noninteractive apt-get update -q 1>/dev/null || exit 1
	DEBIAN_FRONTEND=noninteractive apt-get -yq install $prep_deps 1>/dev/null || exit 1
fi

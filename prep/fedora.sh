if [ -z "$prep_deps" ]; then
	info_log 'no deps specified'
else
	dnf install -yq $prep_deps 1>/dev/null || exit 1
fi

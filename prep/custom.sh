if [ -z "$prep_script" ]; then
	error_log 'custom prep script not specified'
	exit 1
fi

. "$basedir/$prep_script"
cd "$basedir"

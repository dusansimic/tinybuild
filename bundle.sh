#!/bin/sh

cp tinybuild.sh tinybuild.bundle.sh

# Find paths of all files that are sourced in run script
files="$(grep -o '\. ".*"' tinybuild.sh | sed 's~\. "$basedir/\(.*\)"~\1~' | paste -sd " ")"

for f in $files; do
	# Append contents of the file below its sourcing
	sed -i '\~.*'$f'.*~r '$f tinybuild.bundle.sh
	# Remove sourcing of the file
	sed -i '\~.*'$f'.*~d' tinybuild.bundle.sh
done

# tinybuild

> Tiny build system for lazy people

## What?

tinybuild is a simple build system written in POSIX shell (at least trying to be) that is used in
CI/CD for building and packaging programs and applications. It's a set of simple and opinionated
scripts that can easily be extended.

## Why?

I needed a build system for making packages for multiple Linux distributions. Only solutions that I
could find were either large build systems that hade a steep learning curve or were just unpleseant
to work with. Other systems were basically shell scripts run in CI/CD pipeline. I decided to make a
build system that is basically a set of scripts for CI/CD pipeline which can be easily controlled
using environment variables.

_Why do a manual task that takes 10 minutes when you can automate it in 10 hours?_

## Usage

You'll first need to make a `Tinybuildfile` which contains environment variable definitions and
custom function definitions for specific stages if you need them. You can pass the path to the file
as an argument to the script or just let the script load the default path (`./Tinybuildfile`).

To run the script `curl` the `tinybuild.bundle.sh` script into your CI/CD pipeline and and run it
(`sh run.sh`).

If you would like to get colored output, set the `color` variable to any value (it must __not__ be
empty).

Before running anything, there are several variables describing the package that need to be defined.

#### pkgname: string

> Name of the package

#### pkgver: string

> Version of the package

#### pkgrev: string

> Revision of the package

This is optional, default value is `1`.

#### pkgarch: string

> Architecture on which the package is being built

The whole system is split into several stages: prep, source, build and package stage.

### Prep stage

This stage is essentially used to install required packages and prepare the os for the build and
package process. You should take into account that no dependecies are installed by default so if you
want to build an Electron app, you'll need to specify `nodejs` and `npm` packages as deps.

To specify which system you're preping and how, specify the following variables.

#### prep: enum<string>

> Describes which system you're using

Possible values:
* ubuntu
* fedora
* custom

#### prep_deps: string

> Dependancies for building and packaging the program

Space separated names of packages.

#### prep_script: string (custom)

> Path to script that will be used for custom prep stage

Required if using `build=custom`.

### Source stage

In source stage, source files are downloaded and prepared for building and packaging.

#### source: enum<string>

> Describe how the sources will be acquired

Possible values:
* local
* git
* tgz
* file

#### source_path: string (local)

> Set the path for the source in local fs

#### source_repo: string (git)

> Git repo url

#### source_tgz: string (tgz)

> Url of the tar.gz archive

### source_file: string (file)

> Url of the file

### Build stage

Here package is build using predefined scripts in the build system. This stage can be skipped in
order to use some other way of building the package (for example rpmbuild).

#### build: enum<string>

> Describe which build script will be used

Possible values:
* make
* cargo
* electronbuilder
* custom
* skip

On skip option, build process will be skipped.

#### build_makeparams: string (make)

> Parameters for make

#### build_ebparams: string (electronbuilder)

> Parameters for electron builder

#### build function (custom)

A function named `build` needs to be created in `Tinybuildfile` if you are using `build=custom`.

### Package stage

In this stage, built program is packaged into specified distribution package.

The resulting packages are placed into `out` directory.

#### package: enum<string>

> Specify the package script

Possible values:
* tgz
* rpm
* deb
* skip

#### package tgz prep function (tgz)

A function named `prep` needs to be created in `Tinybuildfile` if you are using `package=tgz`. You
should use variables defined by tinybuild script: `pkgdir` and `srcdir`. The `pkgdir` variable
contians a path to the directory where the fs structure for the package should be stored. The
`srcdir` variable contains a path to the source directory where the built files and binaries are
stored.

#### package_rpmpath: string (rpm)

> Path to the rpm spec and sources

To package the program as rpm, you need to break the common way of using the build system. Source
stage is used to fetch the files needed for rpm building. Source of the program is already specifed
in the spec file so there is no need to download it explicitely. Since rpmbuild runs it's own build
process, you can skip the build stage defined in the build system.

Rpm spec and additional sources need to be copied before packaging process begins. To do this, just
specify the path to the directory where those files are located. The spec file needs to be named
`<pacakgename>.spec`.

#### package deb prep function (deb)

Read description about `package tgz prep function`.

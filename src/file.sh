if [ -z "$source_file" ]; then
	error_log 'file url not specified'
fi
cd "$srcdir"
info_log "downloading source to $source_file"
curl -LOs "$source_file" 1>/dev/null || exit 1
cd "$basedir"

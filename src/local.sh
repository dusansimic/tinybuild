if [ -z "$source_path" ]; then
	warn_log 'no path specified, using current path'
	source_path=.
fi
info_log "copying sources from $source_path to src"
cp -r "$source_path"/* "$srcdir"

if [ -z "$source_repo" ]; then
	error_log 'source repo not specified'
	exit 1
fi
info_log "cloning $source_repo into src"
git clone --recurse-submodules "$source_repo" "$srcdir" 1>/dev/null || exit 1

if [ -z "$source_tgz" ]; then
	error_log 'tgz url not specifed'
	exit 1
fi
info_log "downloading $source_tgz"
curl -Ls -o "src.tgz" "$source_tgz" 1>/dev/null || exit 1
tar -xzf "src.tgz" -C "$srcdir" 1>/dev/null || exit 1
rm src.tgz

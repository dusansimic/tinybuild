#!/bin/sh

basedir=$(pwd)
tinybuilderdir="$basedir/.tinybuild"
srcdir="$tinybuilderdir/src"
pkgdir="$tinybuilderdir/pkg"
outdir="$tinybuilderdir/out"

error_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[31m error\033[0m: $1\n"
	else
		printf "[tinybuild] error: $1\n"
	fi
}

info_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[0m info: $1\n"
	else
		printf "[tinybuild] info: $1\n"
	fi
}

warn_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[33m warn\033[0m: $1\n"
	else
		printf "[tinybuild] warn: $1\n"
	fi
}

specpath="./Tinybuildfile"
if [ -n "$1" ]; then
	specpath="$1"
fi
. "$basedir/prep/spec.sh"
info_log 'loaded build spec'

info_log 'create builder dirs'
mkdir -p "$tinybuilderdir"
mkdir -p "$srcdir"
mkdir -p "$pkgdir"
mkdir -p "$outdir"

info_log 'begin builder'

if [ -z "$pkgname" ]; then
	error_log 'package name not specifed'
	exit 1
fi

check_verrevarch() {
	if [ -z "$pkgver" ]; then
		error_log 'package version not specifed'
		exit 1
	fi

	if [ -z "$pkgrev" ]; then
		warn_log 'package version rev not specified'
	fi

	if [ -z "$pkgarch" ]; then
		error_log 'package architecture not specified'
		exit 1
	fi
}

case "$prep" in
	ubuntu)
		info_log 'running prep for ubuntu'
		. "$basedir/prep/ubuntu.sh"
		;;
	fedora)
		info_log 'running prep for fedora'
		. "$basedir/prep/fedora.sh"
		;;
	custom)
		info_log 'running custom prep'
		. "$basedir/prep/custom.sh"
		;;
	*)
		error_log "unknown system for preping specified ($prep)"
		exit 1
		;;
esac

case "$source" in
	local)
		info_log 'fetching source from local'
		. "$basedir/src/local.sh"
		;;
	git)
		info_log 'fetching source from git'
		. "$basedir/src/git.sh"
		;;
	tgz)
		info_log 'fetching source tgz'
		. "$basedir/src/tgz.sh"
		;;
	file)
		info_log 'fetching source file'
		. "$basedir/src/file.sh"
		;;
	*)
		error_log "unknown source fetch method specified ($source)"
		exit 1
		;;
esac

case "$build" in
	make)
		info_log 'running build with make'
		. "$basedir/build/make.sh"
		;;
	cargo)
		info_log 'running build with cargo'
		. "$basedir/build/cargo.sh"
		;;
	electronbuilder)
		info_log 'running build with electron builder'
		. "$basedir/build/electronbuilder.sh"
		;;
	custom)
		info_log 'running custom build'
		. "$basedir/build/custom.sh"
		;;
	skip)
		info_log 'skipping build stage'
		;;
	*)
		error_log "unknown build method specified ($build)"
		exit 1
		;;
esac

case "$package" in
	tgz)
		info_log 'packaging tgz'
		. "$basedir/package/tgz.sh"
		;;
	rpm)
		info_log 'packaging rpm'
		. "$basedir/package/rpm.sh"
		;;
	deb)
		info_log 'packaging deb'
		. "$basedir/package/deb.sh"
		;;
	skip)
		info_log 'skipping package stage'
		;;
	*)
		error_log "unknown packaging method specified ($package)"
		exit 1
		;;
esac

info_log 'returning to base dir'
cd "$basedir"

info_log 'end builder'
